def pal(theme, col, l=None):
  if not col: return 0
  col = col % len(palettes[theme])
  if l != None:
    return light(palettes[theme][col], l)
  return palettes[theme][col]

def light(rgb,l):
  b = rgb & 255
  g = (rgb >> 8) & 255
  r = (rgb >> 16) & 255
  rgb = int(r*l) << 16
  rgb += int(g*l) << 8
  rgb += int(b*l)
  return rgb

def rgb_to_hsv(r, g, b):
  r = float(r)
  g = float(g)
  b = float(b)
  high = max(r, g, b)
  low = min(r, g, b)
  h, s, v = high, high, high
  d = high - low
  s = 0 if high == 0 else d/high
  if high == low:
    h = 0.0
  else:
    h = {
      r: (g - b) / d + (6 if g < b else 0),
      g: (b - r) / d + 2,
      b: (r - g) / d + 4,
    }[high]
    h /= 6
  return h, s, v

def hsv_to_rgb(h, s, v):
  i = int(h*6)
  f = h*6 - i
  p = v * (1-s)
  q = v * (1-f*s)
  t = v * (1-(1-f)*s)
  r, g, b = [
    (v, t, p),
    (q, v, p),
    (p, v, t),
    (p, q, v),
    (t, p, v),
    (v, p, q),
  ][int(i%6)]
  rgb = int(r*255) << 16
  rgb += int(g*255) << 8
  rgb += int(b*255)
  return rgb

palettes = {
  4:(
    0x222222,
    0x888888,
    0x555555,
    0xbbbbbb,
  ),
  12:(
    0x0000ff,
    0x0088ff,
    0x00ffff,
    0x00ff88,
    0x00ff00,
    0x88ff00,
    0xffff00,
    0xff8800,
    0xff0000,
    0xff0088,
    0xff00ff,
    0x8800ff,
    ),
  24:(
    0x0000ff,
    0x0033ff,
    0x0077ff,
    0x00bbff,
    0x00ffff,
    0x00ffbb,
    0x00ff77,
    0x00ff33,
    0x00ff00,
    0x33ff00,
    0x77ff00,
    0xbbff00,
    0xffff00,
    0xffbb00,
    0xff7700,
    0xff3300,
    0xff0000,
    0xff0033,
    0xff0077,
    0xff00bb,
    0xff00ff,
    0xbb00ff,
    0x7700ff,
    0x3300ff,
    ),
  48:(
    0x0000ff,
    0x0011ff,
    0x0033ff,
    0x0055ff,
    0x0077ff,
    0x0099ff,
    0x00bbff,
    0x00ddff,
    0x00ffff,
    0x00ffee,
    0x00ffcc,
    0x00ffaa,
    0x00ff88,
    0x00ff66,
    0x00ff44,
    0x00ff22,
    0x00ff00,
    0x11ff00,
    0x33ff00,
    0x55ff00,
    0x77ff00,
    0x99ff00,
    0xbbff00,
    0xddff00,
    0xffff00,
    0xffee00,
    0xffcc00,
    0xffaa00,
    0xff8800,
    0xff6600,
    0xff4400,
    0xff2200,
    0xff0000,
    0xff0011,
    0xff0033,
    0xff0055,
    0xff0077,
    0xff0099,
    0xff00bb,
    0xff00dd,
    0xff00ff,
    0xee00ff,
    0xcc00ff,
    0xaa00ff,
    0x8800ff,
    0x6600ff,
    0x4400ff,
    0x2200ff,
    ),
  }
