keys = {
  # top keys
  "apps":0, "symb":1, "up":2, "help":3, "esc":4, "home":5, "plot":6,
  "left":7, "right":8, "view":9, "cas":10, "num":11, "down":12,
  "menu":13,
  # orange
  "a":14, "b":15, "c":16, "d":17, "e":18, "del":19, "f":20, "g":21,
  "h":22, "i":23, "j":24, "k":25, "l":26, "m":27, "n":28, "o":29,
  "enter":30, "p":31, "q":32, "r":33, "s":34, "t":35, "alpha":36,
  "u":37, "v":38, "w":39, "x":40, "shift":41, "y":42, "z":43,
  "hash":44, "doubledot":45, "off":46, "notes":47, "equal":48,
  "underscore":49, "ans":50,
  # nums
  "1":42, "2":43, "3":44, "4":37, "5":38, "6":39, "7":32, "8":33,
  "9":34, "0":47, "/":35, "*":40, "-":45, "+":50,
}

width = 320
height = 240 # hp prime screen pixels
