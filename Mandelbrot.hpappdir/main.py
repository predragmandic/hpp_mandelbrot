### Mandelbrot #####################################################
# Version: 0.1
# Date: 2022-01-06
# Author: Predrag Mandic (mushman) <predrag@nul.one>
# License: BSD-3: https://opensource.org/licenses/BSD-3-Clause
####################################################################

# PRESS [HELP] TO VIEW KEYBOARD SHORTCUTS

from graphic import *
from hpprime import *
from math import *
from palettes import pal, light, hsv_to_rgb
import prime
import sys

window = None
mandelbrot_window = None
julia_window = None

def julia(pos, mark, max_iter):
  z = pos
  c = mark
  if abs(z) > 2: return 1
  for i in range(max_iter):
    z = z**2 + c
    if abs(z) > 2: return i+1
  return 0

def mandelbrot(pos, mark, max_iter):
  z = pos
  c = pos
  if abs(z) > 2: return 1
  for i in range(max_iter):
    z = z**2 + c
    if abs(z) > 2: return i+1
  return 0

class ComplexWindow:
  def __init__(self, width, height, calc, max_iter, events, colorize):
    """
    width, hight - Screen size in pixels.
    calc - Function that calculates a value for a given point(s).
    max_iter - Maximum iterations for calc.
    events - Function that handles keyboard events.
    colorize - Function that returns a list of colors.
    """
    self.width = width
    self.height = height
    self.calc = calc
    self.max_iter = max_iter
    self.events = events
    self.colorize = colorize
    self.pos = complex(0,0)
    self.mark = None
    self.zoom = 0.32
    self.move_pixels = self.width//4
    self.iter_delta = 8
    self.clear()
    self.setup()
  def move_left(self):
    grid_plus = [ [-1]*len(self.grid[0]) for n in range(self.move_pixels) ]
    grid_plus.extend(self.grid[:-self.move_pixels])
    self.grid = grid_plus
    self.pos += complex(-self.delta*self.move_pixels, 0)
    self.setup()
  def move_right(self):
    grid_plus = [ [-1]*len(self.grid[0]) for n in range(self.move_pixels) ]
    self.grid = self.grid[self.move_pixels:]
    self.grid.extend(grid_plus)
    self.pos += complex(self.delta*self.move_pixels, 0)
    self.setup()
  def move_up(self):
    for i in range(len(self.grid)):
      grid_plus = [-1]*(self.move_pixels)
      grid_plus.extend(self.grid[i][:-self.move_pixels])
      self.grid[i] = grid_plus
    self.pos += complex(0,-self.delta*self.move_pixels)
    self.setup()
  def move_down(self):
    for i in range(len(self.grid)):
      grid_plus = [-1]*(self.move_pixels)
      self.grid[i] = self.grid[i][self.move_pixels:]
      self.grid[i].extend(grid_plus)
    self.pos += complex(0,self.delta*self.move_pixels)
    self.setup()
  def zoom_in(self, times=None):
    self.zoom *= 2
    self.clear()
    self.setup()
  def zoom_out(self):
    self.zoom /= 2
    self.clear()
    self.setup()
  def iter_up(self):
    self.max_iter += self.iter_delta
    for i in range(len(self.grid)):
      for j in range(len(self.grid[0])):
        if self.grid[i][j] == 0:
          self.grid[i][j] = -1
    self.setup()
  def iter_down(self):
    self.max_iter -= self.iter_delta
    if self.max_iter < self.iter_delta: self.max_iter = self.iter_delta
    for i in range(len(self.grid)):
      for j in range(len(self.grid[0])):
        if self.grid[i][j] > self.max_iter:
          self.grid[i][j] = 0
    self.setup()
  def clear(self):
    self.grid = None
    self.grid = [ [-1]*self.height for n in range(self.width) ]
  def setup(self):
    self.colors = list(map(self.colorize(self.max_iter), range(self.max_iter+1)))
    self.delta = 1/self.width/self.zoom
    self.c0 = complex(
      self.pos.real - self.width/2*self.delta,
      self.pos.imag - self.height/2*self.delta
      )
  def reset(self):
    self.zoom = .32
    self.pos = complex(0,0)
    self.clear()
    self.setup()
  def xy_to_c(self, x, y):
    return complex(
      self.c0.real + x*self.delta,
      self.c0.imag + y*self.delta
      )
  def calc_value(self, x,y):
    "Get or calculate grid point value."
    val = self.grid[x][y]
    if val != -1:
      return val
    val = self.calc(self.xy_to_c(x,y), self.mark, self.max_iter)
    self.grid[x][y] = val
    return val
  def __low_res(self, p, skip):
    "Draw reduced resolution plot."
    for y in range(self.height/p):
      if self.events(): return True
      for x in range(self.width/p):
        if skip and not (x%2 + y%2): continue
        val = self.calc_value(x*p,y*p)
        color = self.colors[val]
        if p == 1:
          pixon(0,x*p,y*p,color)
          continue
        fillrect(0,x*p,y*p,p,p,color,color)
    return False
  def draw(self):
    self.setup()
    if self.__low_res(32, skip=False): return True
    for p in (16, 8, 4, 2, 1):
      if self.__low_res(p, skip=True): return True
    return False

def keys():
  """Check for key events and initiate key action.
  Returns True if event was handled.
  """
  global window, mandelbrot_window, julia_window
  key = eval("getkey")
  if key == -1.0: return False
  elif key == prime.keys["enter"]:
    if window == mandelbrot_window:
      window = julia_window
      if julia_window.mark != mandelbrot_window.pos:
        window.mark = mandelbrot_window.pos
        window.reset()
    elif window == julia_window:
      window = mandelbrot_window
    else:
      raise("Not sure which window.")
  elif key == prime.keys["esc"]: sys.exit(0)
  elif key == prime.keys["home"]: sys.exit(0)
  elif key == prime.keys["apps"]: sys.exit(0)
  elif key == prime.keys["left"]: window.move_left()
  elif key == prime.keys["right"]: window.move_right()
  elif key == prime.keys["up"]: window.move_up()
  elif key == prime.keys["down"]: window.move_down()
  elif key == prime.keys["+"]: window.zoom_in()
  elif key == prime.keys["-"]: window.zoom_out()
  elif key == prime.keys["*"]: window.iter_up()
  elif key == prime.keys["/"]: window.iter_down()
  elif key == prime.keys["0"]: window.colorize=build_colorize(0)
  elif key == prime.keys["1"]: window.colorize=build_colorize(1)
  elif key == prime.keys["2"]: window.colorize=build_colorize(2)
  elif key == prime.keys["3"]: window.colorize=build_colorize(3)
  elif key == prime.keys["4"]: window.colorize=build_colorize(4)
  elif key == prime.keys["5"]: window.colorize=build_colorize(5)
  elif key == prime.keys["6"]: window.colorize=build_colorize(6)
  elif key == prime.keys["7"]: window.colorize=build_colorize(7)
  elif key == prime.keys["8"]: window.colorize=build_colorize(8)
  elif key == prime.keys["9"]: window.colorize=build_colorize(9)
  elif key == prime.keys["help"]: usage()
  elif key == prime.keys["k"]: info(window)
  else: return False
  return True

def usage():
  "Print key assignments."
  print("\n\nKeys:\n")
  print("arrows  move graph")
  print("+ -     change zoom")
  print("* /     change max iterations")
  print("0-9     select color theme")
  print("Enter   toggle Julia set")
  print("LOG     print current coordinates")
  print("Esc     quit")
  print("\nany key to return...")
  while eval("getkey") == -1.0:
    pass

def info(window):
  "Print position and zoom info."
  print("\n\n---")
  print("real: " + str(window.pos.real))
  print("imag: " + str(window.pos.imag))
  print("zoom: " + str(window.zoom))
  if window.mark is not None: print("mark: " + str(window.mark))
  print("max_iter: " + str(window.max_iter))
  print("---")
  print("\nany key to return...")
  while eval("getkey") == -1.0:
    pass

def build_colorize(t):
  "Return function that generates colors based on max_iter and iter."
  f = None
  if t == 0:
    f = lambda max_iter: lambda iter: hsv_to_rgb(.8*(1-sqrt(iter/max_iter)), 1, 0.2+0.8*iter/max_iter if iter else 0)
  elif t == 1:
    f = lambda max_iter: lambda iter: pal(48,iter,iter/max_iter)
  elif t == 2:
    f = lambda max_iter: lambda iter: pal(24,iter,iter/max_iter)
  elif t == 3:
    f = lambda max_iter: lambda iter: pal(12,iter,iter/max_iter)
  elif t == 4:
    f = lambda max_iter: lambda iter: pal(48,-iter,1-iter/max_iter)
  elif t == 5:
    f = lambda max_iter: lambda iter: pal(24,iter,1-iter/max_iter)
  elif t == 6:
    f = lambda max_iter: lambda iter: pal(12,iter,1-iter/max_iter)
  elif t == 7:
    f = lambda max_iter: lambda iter: int(0xff*iter/max_iter)*0x010101
  elif t == 8:
    f = lambda max_iter: lambda iter: pal(4,iter)
  elif t == 9:
    f = lambda max_iter: lambda iter: 0 if iter else 0xffffff
  return f

mandelbrot_window = ComplexWindow(
  width=prime.width,
  height=prime.height,
  max_iter=40,
  calc=mandelbrot,
  events=keys,
  colorize=build_colorize(0)
  )
mandelbrot_window.pos = complex(-.5,0)
mandelbrot_window.zoom = 0.32
mandelbrot_window.setup()

julia_window = ComplexWindow(
  width=prime.width,
  height=prime.height,
  max_iter=40,
  calc=julia, 
  events=keys,
  colorize=build_colorize(0)
  )
window = mandelbrot_window

while window.draw(): pass
while True:
  if keys():
    while window.draw(): pass
