Mandelbrot
==================================================

Application for HP Prime calculator that draws Mandelbrot and Julia sets.

Installation
-------------------------

### Requirements
- HP Connectivity Kit
- HP Prime calculator with Python

Copy `Mandelbrot.hpappdir` directory into `Application Library` of the real or emulated calculator using HP Connectivity Kit.

Usage
-------------------------

Mandelbrot app is a python app. Once you open it, tap on "Clear" button to start/restart it. It will draw the Mandelbrot set initially.

### Controls

- Arrow keys: Move around
- `+`,`-`: Zoom in/out
- `*`,`/`: Increase/decrease maximum iterations by 8
- `0`-`9`: Change color theme
- `Enter`: Toggle Julia set based on complex number that was in the center of the screen when in Mandelbrot set.
- `LOG`: Print info about current coordinates, zoom level, etc.
- `ESC`,`Apps`,`Home`: Quit

Implementation info
-------------------------

Set is calculated for points in the visible bounds of complex plane, one point per pixel of the screen. Initially the resolution is reduced by factor of 32 to give a quicker rough display of the set. Next, the set is calculated for resolution reduced by 16,8,4 and 2 to build better picture progressively with each pass. Points that are already calculated are not re-calculated in subsequent passes.

Once each point is calculated (value is number of iterations before it passes maximum or becomes bigger than abs(z)==2) it's stored in a 2d array. When moving around, the array is being shifted and new area is recalculated only (to speed things up) and visible area that is not visible any more is dropped from the array (to preserve memory). When the color theme is changed, values from the array are being redrawn on the screen without recalculating. When changing zoom, the whole array is dropped and calculated again. I tried optimizing, e.g. saving every 2nd pixel when zooming 2 times in/out, but it seems that I was hitting memory limit and made app crash if I introduced second array. Manipulating existing array when zooming in proved to be slow and gave worse feel than showing low-res preview when I wanted to zoom couple of times quickly.

Julia set works in the same manner as Mandelbrot set (same ComplexWindow class) and is simply drawn over Mandelbrot set when toggle button is hit.

When maximum number of iterations is increased, only the parts of mandelbrot/julia array that previously had value of 0 is recalculated.

Because of lack of Numpy or any similar library, only basic python arrays and complex number implementations are used.

Tested on HP Prime G2, software version 2.1.14603 (2021-12-02), OS V2.060.650

